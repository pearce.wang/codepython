This is a test project for git and JIRA


Here I some more lines to demostrate it.

remove some lines while add some lines

Overview
Google's framework for writing and using C++ mock classes. It can help you derive better designs of your system and write better tests.

It is inspired by:

jMock,
EasyMock, and
Hamcrest,
and designed with C++'s specifics in mind.

Google mock:

lets you create mock classes trivially using simple macros.
supports a rich set of matchers and actions.
handles unordered, partially ordered, or completely ordered expectations.
is extensible by users.
We hope you find it useful!

Modified here


Features
Provides a declarative syntax for defining mocks.
Can easily define partial (hybrid) mocks, which are a cross of real and mock objects.
Handles functions of arbitrary types and overloaded functions.
Comes with a rich set of matchers for validating function arguments.
Uses an intuitive syntax for controlling the behavior of a mock.
Does automatic verification of expectations (no record-and-replay needed).

add some line